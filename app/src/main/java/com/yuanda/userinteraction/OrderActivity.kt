package com.yuanda.userinteraction

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.yuanda.userinteraction.databinding.ActivityOrderBinding

class OrderActivity : AppCompatActivity() {
    private lateinit var binding: ActivityOrderBinding
    private val KUNCI = "kunciOrder"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityOrderBinding.inflate(layoutInflater)
        val view = binding.root
        setContentView(view)

        var pesan = "Anda Memesan" + intent.getStringExtra(KUNCI)
        binding.orderText.text = pesan
    }
}