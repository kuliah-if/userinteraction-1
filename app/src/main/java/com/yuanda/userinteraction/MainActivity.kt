package com.yuanda.userinteraction

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Toast
import com.yuanda.userinteraction.databinding.ActivityMainBinding

class MainActivity : AppCompatActivity() {
    private lateinit var binding: ActivityMainBinding
    private var pilihan: String = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        val view = binding.root
        setContentView(view)

        binding.imgKapucino.setOnClickListener { view -> run { fButton(view) } }
        binding.imgKopi.setOnClickListener { view -> run { fButton(view) } }
        binding.imgTeh.setOnClickListener { view -> run { fButton(view) } }

        binding.fabOrder.setOnClickListener { view -> kotlin.run {
            val KUNCI = "kunciOrder"
            val intOrder = Intent(this, OrderActivity::class.java)
            intOrder.putExtra(KUNCI, pilihan)
            startActivity(intOrder)
        } }
    }

    private fun tampilToast(text: String) {
        Toast.makeText(applicationContext, text, Toast.LENGTH_SHORT).show()
    }

    private fun fButton(view: View) {
        pilihan = view.contentDescription.toString()
        tampilToast("${pilihan} terpilih")
    }
}